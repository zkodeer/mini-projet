FROM nginx:1.21.1
LABEL maintainer="Zoe KODEER"

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y curl && \
    apt-get install -y git

RUN rm -Rf /usr/share/nginx/html/*

# Clone du dépôt et récupération de la branche "featur"
RUN git clone https://gitlab.com/zkodeer/projet-web.git /usr/share/nginx/html && \
    cd /usr/share/nginx/html && \
    git checkout featur

COPY nginx.conf /etc/nginx/conf.d/default.conf
CMD sed -i -e "s/\$PORT/$PORT/g" /etc/nginx/conf.d/default.conf && nginx -g 'daemon off;'
